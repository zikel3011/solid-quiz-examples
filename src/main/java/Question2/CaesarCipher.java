package Question2;

public class CaesarCipher implements Cipher {

    private final int numberOfRotations;
    private static final int LOWER_CASE_A = 65;
    private static final int UPPER_CASE_A = 97;


    public CaesarCipher(int numberOfRotations) {
        this.numberOfRotations = numberOfRotations;
    }

    @Override
    public String encrypt(String textToEncrypt) {
        return generateCipherResult(textToEncrypt, this.numberOfRotations);
    }

    @Override
    public String decrypt(String textToDecrypt) {
        return generateCipherResult(textToDecrypt, -this.numberOfRotations);
    }

    private String generateCipherResult(String textToCipher, int numberOfRotations){
        StringBuilder encryptedText = new StringBuilder();
        for (char letter : textToCipher.toCharArray()) {
            encryptedText.append(rotateLetter(letter,numberOfRotations));
        }
        return encryptedText.toString();
    }

    private char rotateLetter(char letter, int numberOfRotations) {
        int asciiFirstLetter;
        asciiFirstLetter = getAsciiFirstLetter(letter);
        return (char) ((letter + numberOfRotations - asciiFirstLetter) % 26 + asciiFirstLetter);
    }

    private int getAsciiFirstLetter(char letter) {
        if (Character.isUpperCase(letter))
            return LOWER_CASE_A;
        else
            return UPPER_CASE_A;
    }
}
