package CompleteExample;

public class CaesarCipher implements Cipher {

    private final int numberOfRotations;
    LetterRotator letterRotator = new LetterRotator();

    public CaesarCipher(int numberOfRotations) {
        this.numberOfRotations = numberOfRotations;
    }

    @Override
    public String encrypt(String textToEncrypt) {
        return generateCipherResult(textToEncrypt, this.numberOfRotations);
    }

    @Override
    public String decrypt(String textToDecrypt) {
        return generateCipherResult(textToDecrypt, -this.numberOfRotations);
    }

    private String generateCipherResult(String textToCipher, int numberOfRotations){
        StringBuilder encryptedText = new StringBuilder();
        for (char letter : textToCipher.toCharArray()) {
            encryptedText.append(letterRotator.rotateLetter(letter,numberOfRotations));
        }
        return encryptedText.toString();
    }
}
